import EatKit
import EatCommon
import EatNetworking

class StartDependencyContainer {
    private let keychainManager: KeychainManager
    private let networkRepository: NetworkRepository
    private let userDefaultsManager: UserDefaultsManager
    
    init(keychainManager: KeychainManager, networkRepository: NetworkRepository, userDefaultsManager: UserDefaultsManager) {
        self.keychainManager = keychainManager
        self.networkRepository = networkRepository
        self.userDefaultsManager = userDefaultsManager
    }
    
    func makeStartViewModel() -> StartViewModel {
        return StartViewModel(keychainManager: self.keychainManager, networkRepository: self.networkRepository, userDefaultsManager: self.userDefaultsManager)
    }
    
    func makeStartView(viewModel: StartViewModel) -> StartView {
        return StartView(viewModel: viewModel)
    }
    
    func makeStartViewController(rootView: StartView, viewModel: StartViewModel) -> StartViewController {
        return StartViewController(rootView: rootView, viewModel: viewModel)
    }
    
    func makeLocationsDependencyContainer() -> LocationsDependencyContainer {
        return LocationsDependencyContainer()
    }
    
    func makeMainDependencyContainer() -> MainDependencyContainer {
        return MainDependencyContainer()
    }
}
