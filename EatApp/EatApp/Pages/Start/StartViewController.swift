import EatKit

class StartViewController: NiblessViewController {
    let rootView: StartView
    let viewModel: StartViewModel
    
    init(rootView: StartView, viewModel: StartViewModel) {
        self.rootView = rootView
        self.viewModel = viewModel
        super.init()
    }
    
    override func loadView() {
        super.loadView()
        self.view = self.rootView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.setupViewModel()
    }
}
