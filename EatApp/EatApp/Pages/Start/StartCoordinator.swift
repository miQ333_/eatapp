import Combine
import EatCommon

class StartCoordinator: BaseCoordinator {
    private let dependencyContainer: StartDependencyContainer
    
    init(dependencyContainer: StartDependencyContainer) {
        self.dependencyContainer = dependencyContainer
        super.init()
    }
    
    override func start() {
        let viewModel = self.dependencyContainer.makeStartViewModel()
        viewModel.navigationPublisher
            .sink(receiveValue: { [weak self] destination in
                guard let strongSelf = self else { return }
                switch destination {
                case .main(let location):
                    strongSelf.showMainView(location: location)
                case .locations:
                    strongSelf.showLocationPickerView()
                }
            })
            .store(in: &self.subscriptions)
        let rootView = self.dependencyContainer.makeStartView(viewModel: viewModel)
        let viewController = self.dependencyContainer.makeStartViewController(rootView: rootView, viewModel: viewModel)
            
        self.navigationController.viewControllers = [viewController]
    }
    
    private func showMainView(location: Location) {
        let dependencyContainer = self.dependencyContainer.makeMainDependencyContainer()
        let coordinator = MainCoordinator(dependencyContainer: dependencyContainer)
        coordinator.navigationController = navigationController
        self.start(coordinator: coordinator)
    }
    
    private func showLocationPickerView() {
        let dependencyContainer = self.dependencyContainer.makeLocationsDependencyContainer()
        let coordinator = LocationsCoordinator(dependencyContainer: dependencyContainer)
        coordinator.navigationController = navigationController
        self.start(coordinator: coordinator)
    }
}
