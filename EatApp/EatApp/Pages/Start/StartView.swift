import EatKit
import UIKit
import Combine

class StartView: NiblessView {
    let viewModel: StartViewModel
    var subscriptions = Set<AnyCancellable>()
    
    let retryButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.white, for: .normal)
        button.setTitle(Localizable.tryAgain, for: .normal)
        button.isHidden = true
        return button
    }()

    let appLogo: UIImageView = {
        let logo = UIImageView(image: ImageRepository.appLogoImage)
        logo.contentMode = .scaleAspectFit
        return logo
    }()
    
    init(viewModel: StartViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        self.backgroundColor = UIColor.EatTheme.green1
        self.setupSubviews()
        self.setupConstraints()
    }
    
    private func setupSubviews() {
        self.setSubviewsForAutoLayout([self.appLogo, self.retryButton])
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            self.appLogo.centerXAnchor.constraint(equalTo: centerXAnchor),
            self.appLogo.centerYAnchor.constraint(equalTo: centerYAnchor),
            self.appLogo.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 100),
            self.appLogo.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -100),
            self.appLogo.heightAnchor.constraint(equalTo: appLogo.widthAnchor),

            self.retryButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            self.retryButton.topAnchor.constraint(equalTo: appLogo.bottomAnchor, constant: 50),
        ])
    }
    
    private func bindViewModel() {
        self.viewModel.errorMessagePublisher
            .sink(receiveValue: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.retryButton.isHidden = false
            })
            .store(in: &self.subscriptions)
    }
    
    private func wireView() {
        self.retryButton.publisher(for: .touchUpInside)
            .sink(receiveValue: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.viewModel.setupViewModel()
            })
            .store(in: &self.subscriptions)
        
    }
}
