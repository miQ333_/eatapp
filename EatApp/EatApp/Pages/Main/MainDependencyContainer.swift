import EatKit

class MainDependencyContainer {
    init() {}
    
    func makeMainViewModel() -> MainViewModel {
        return MainViewModel()
    }
    
    func makeMainView(viewModel: MainViewModel) -> MainView {
        return MainView(viewModel: viewModel)
    }
    
    func makeMainViewController(rootView: MainView, viewModel: MainViewModel) -> MainViewController {
        return MainViewController(rootView: rootView, viewModel: viewModel)
    }
}
