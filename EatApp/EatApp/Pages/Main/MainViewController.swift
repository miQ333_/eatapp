import EatKit

class MainViewController: NiblessViewController {
    private let rootView: MainView
    private let viewModel: MainViewModel
    
    init(rootView: MainView, viewModel: MainViewModel) {
        self.rootView = rootView
        self.viewModel = viewModel
        super.init()
    }
    
    override func loadView() {
        super.loadView()
        self.view = self.rootView
    }
}
