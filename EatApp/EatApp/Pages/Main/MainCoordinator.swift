import Combine
import EatCommon

class MainCoordinator: BaseCoordinator {
    private let dependencyContainer: MainDependencyContainer
    
    init(dependencyContainer: MainDependencyContainer) {
        self.dependencyContainer = dependencyContainer
        super.init()
    }
    
    override func start() {
        let viewModel = self.dependencyContainer.makeMainViewModel()
        let rootView = self.dependencyContainer.makeMainView(viewModel: viewModel)
        let viewController = self.dependencyContainer.makeMainViewController(rootView: rootView, viewModel: viewModel)
            
        self.navigationController.viewControllers = [viewController]
    }
}
