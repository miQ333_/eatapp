import Combine
import EatCommon

class LocationsCoordinator: BaseCoordinator {
    private let dependencyContainer: LocationsDependencyContainer
    
    init(dependencyContainer: LocationsDependencyContainer) {
        self.dependencyContainer = dependencyContainer
        super.init()
    }
    
    override func start() {
        let viewModel = self.dependencyContainer.makeLocationPickerViewModel()
        let rootView = self.dependencyContainer.makeLocationPickerView(viewModel: viewModel)
        let viewController = self.dependencyContainer.makeLocationPickerViewController(rootView: rootView, viewModel: viewModel)
            
        self.navigationController.viewControllers = [viewController]
    }
}
