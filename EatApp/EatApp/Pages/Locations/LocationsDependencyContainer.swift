import EatKit
class LocationsDependencyContainer {
    init() {}
    
    func makeLocationPickerViewModel() -> LocationPickerViewModel {
        return LocationPickerViewModel()
    }
    
    func makeLocationPickerView(viewModel: LocationPickerViewModel) -> LocationPickerView {
        return LocationPickerView(viewModel: viewModel)
    }
    
    func makeLocationPickerViewController(rootView: LocationPickerView, viewModel: LocationPickerViewModel) -> LocationPickerViewController {
        return LocationPickerViewController(rootView: rootView, viewModel: viewModel)
    }
}
