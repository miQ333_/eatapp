import EatKit

class LocationPickerViewController: NiblessViewController {
    private let rootView: LocationPickerView
    private let viewModel: LocationPickerViewModel
    
    init(rootView: LocationPickerView, viewModel: LocationPickerViewModel) {
        self.rootView = rootView
        self.viewModel = viewModel
        super.init()
    }
    
    override func loadView() {
        super.loadView()
        self.view = self.rootView
    }
}
