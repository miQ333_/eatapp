import EatKit

class LocationPickerView: NiblessView {
    let viewModel: LocationPickerViewModel
    
    init(viewModel: LocationPickerViewModel) {
        self.viewModel = viewModel
        super.init()
    }
}
