import EatKit
import EatCommon
import EatNetworking

class LaunchDependencyContainer {
    private let appEnvironment: AppEnvironment
    private var sharedNetworkRepository: NetworkRepository?
    private var sharedKeychainManager: KeychainManager?
    private var sharedUserDefaultsManager: UserDefaultsManager?
    
    init(appEnvironment: AppEnvironment) {
        self.appEnvironment = appEnvironment
        self.sharedKeychainManager = self.makeEatKeychainManager()
        self.sharedUserDefaultsManager = self.makeEatUserDefaultsManager()
        self.sharedNetworkRepository = self.makeEatNetworkRepository(userDefaultsManager: self.sharedUserDefaultsManager!, baseURL: self.appEnvironment.baseURL, keychainManager: self.sharedKeychainManager!)
//        func makeUserDefaultsManager() -> UserDefaultsManager {
//            return SignalUserDefaultsManager()
//        }
//
//        func makeKeychainManager() -> KeychainManager {
//            return SignalKeychainManager(appName: appEnvironment.getAppName())
//        }
//
//        let userDefaultsManager = makeUserDefaultsManager()
//        let keychainManager = makeKeychainManager()
//
//        func makeTokenManager() -> TokenManager {
//            return SignalTokenManager(keychainManager: keychainManager)
//        }
//
//        func makeNetworkRepository() -> NetworkRepository {
//            return SignalNetworkRepository(networkURL: appEnvironment.getNetworkURL(), appName: appEnvironment.getAppName(), userDefaultsManager: userDefaultsManager, keychainManager: keychainManager, errorManager: makeErrorManager(), tokenManager: makeTokenManager(), reachabilityManager: makeReachabilityManager())
//        }
//
//        let networkRepository = makeNetworkRepository()
//
//        self.sharedAppEnvironment = appEnvironment
//        self.sharedUserDefaultsManager = userDefaultsManager
//        self.sharedKeychainManager = keychainManager
//        self.sharedNetworkRepository = networkRepository
//        self.sharedPushNotificationsManager = makePushNotificationManager()
    }
    
    func makeEatKeychainManager() -> EatKeychainManager {
        return EatKeychainManager(appName: self.appEnvironment.appName)
    }
    
    func makeEatUserDefaultsManager() -> EatUserDefaultsManager {
        return EatUserDefaultsManager()
    }
    
    func makeEatNetworkRepository(userDefaultsManager: UserDefaultsManager, baseURL: String, keychainManager: KeychainManager) -> EatNetworkRepository {
        return EatNetworkRepository(userDefaultsManager: userDefaultsManager, baseURL: baseURL, keychainManager: keychainManager)
    }
    
    func makeStartDependencyContainer() -> StartDependencyContainer {
        return StartDependencyContainer(keychainManager: self.sharedKeychainManager!, networkRepository: self.sharedNetworkRepository!, userDefaultsManager: self.sharedUserDefaultsManager!)
    }
}
