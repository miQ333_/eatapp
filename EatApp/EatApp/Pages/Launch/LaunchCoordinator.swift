import UIKit
import EatKit

class LaunchCoordinator: BaseCoordinator {
    let window: UIWindow
    
    let appDependencyContainer: LaunchDependencyContainer
    
    init(window: UIWindow) {
        self.window = window
        self.appDependencyContainer = LaunchDependencyContainer(appEnvironment: EatAppEnvironment())
        super.init()
    }
    
    override func start() {
        self.navigationController.navigationBar.isHidden = true
        self.window.rootViewController = self.navigationController
        
        let startDependencyContainer = self.appDependencyContainer.makeStartDependencyContainer()

        let coordinator = StartCoordinator(dependencyContainer: startDependencyContainer)
        coordinator.navigationController = self.navigationController
        self.start(coordinator: coordinator)
    }
}
