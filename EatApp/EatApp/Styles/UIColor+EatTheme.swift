import UIKit

extension UIColor {
    struct EatTheme {
        static var greyBackground: UIColor  { return UIColor(hex: 0xF1F2F5) }
        static var greenNotification: UIColor { return UIColor(hex: 0x6DCA87) }
        static var redNotification: UIColor { return UIColor(hex: 0xC86D35) }
        static var blueNotification: UIColor { return UIColor(hex: 0x97CAFA) }
        static var greenSubscription: UIColor { return UIColor(hex: 0x6DCA88) }
        static var greySubscription: UIColor { return UIColor(hex: 0xA2A2A2) }
        static var greyText: UIColor { return UIColor(hex: 0xD3D2D3) }
        static var green1: UIColor { return UIColor(hex: 0x3DCE85) }
    }

    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(hex: Int) {
        self.init(
                red: (hex >> 16) & 0xFF,
                green: (hex >> 8) & 0xFF,
                blue: hex & 0xFF
        )
    }
}
