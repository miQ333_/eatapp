import UIKit

class ImageRepository {
    static let notificationImage = UIImage(named: "notificationIcon")
    static let carImage = UIImage(named: "carIcon")
    static let exitImage = UIImage(named: "exitIcon")
    static let emptyAvatarImage = UIImage(named: "emptyAvatarImage")
    static let appLogoImage = UIImage(named: "appImage")
    static let accessoryImage = UIImage(named: "accessoryIcon")
}
