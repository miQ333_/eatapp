import Foundation

struct Localizable {
    private init() {}
}
public func getString(_ stringId: String) -> String {
    return NSLocalizedString(stringId, comment: "")
}

extension Localizable {
    static let whereAreYou = NSLocalizedString("whereAreYou", comment: "")
    static let lackOfLocations = NSLocalizedString("lackOfLocations", comment: "")
    static let failure = NSLocalizedString("failure", comment: "")
    static let checkYourInternetConnection = NSLocalizedString("checkYourInternetConnection", comment: "")
    static let unknownErrorOccurred = NSLocalizedString("unknownErrorOccurred", comment: "")
    static let new = NSLocalizedString("new", comment: "")
    static let notifications = NSLocalizedString("notifications", comment: "")
    static let noNotificationsLastWeek = NSLocalizedString("noNotificationsLastWeek", comment: "")
    static let clickAndTurnOnNotificationsInYourApp = NSLocalizedString("clickAndTurnOnNotificationsInYourApp", comment: "")
    static let clickAndTurnOnNotifications = NSLocalizedString("clickAndTurnOnNotifications", comment: "")
    static let yesterday = NSLocalizedString("yesterday", comment: "")
    static let settings = NSLocalizedString("settings", comment: "")
    static let today = NSLocalizedString("today", comment: "")
    static let unsubscribeFromCurrentLocation = NSLocalizedString("unsubscribeFromCurrentLocation", comment: "")
    static let pushNotifications = NSLocalizedString("pushNotifications", comment: "")
    static let tryAgain = NSLocalizedString("tryAgain", comment: "")
    static let suppliers = NSLocalizedString("suppliers", comment: "")
    static let subscribed = NSLocalizedString("subscribed", comment: "")
    static let subscribe = NSLocalizedString("subscribe", comment: "")
    static let retry = NSLocalizedString("retry", comment: "")
}
