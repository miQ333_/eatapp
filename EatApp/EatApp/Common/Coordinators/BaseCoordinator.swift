import UIKit
import Combine

public class BaseCoordinator {
    var subscriptions = Set<AnyCancellable>()
    
    var childCoordinators: [BaseCoordinator] = []
    var parentCoordinator: BaseCoordinator?
    var navigationController = NavigationController()
    
    func start() {
        fatalError("Start method must be implemented")
    }
    
    func start(coordinator: BaseCoordinator) {
        self.childCoordinators.append(coordinator)
        coordinator.parentCoordinator = self
        coordinator.start()
    }
    
    func didFinish(coordinator: BaseCoordinator) {
        if let index = self.childCoordinators.firstIndex(where: { $0 === coordinator }) {
            for childCoordinator in self.childCoordinators[index].childCoordinators {
                coordinator.didFinish(coordinator: childCoordinator)
            }
            self.childCoordinators.remove(at: index)
        }
    }
}

class NavigationController: UINavigationController {
     override var childForStatusBarStyle: UIViewController? {
         return visibleViewController
     }
 }
