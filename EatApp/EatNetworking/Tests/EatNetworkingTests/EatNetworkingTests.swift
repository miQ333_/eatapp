import XCTest
@testable import EatNetworking

final class EatNetworkingTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(EatNetworking().text, "Hello, World!")
    }
}
