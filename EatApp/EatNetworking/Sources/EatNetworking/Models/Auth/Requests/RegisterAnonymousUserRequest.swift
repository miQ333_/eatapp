public struct RegisterAnonymousUserRequest: Codable {
    public let platformType: String
    
    public init(platformType: String) {
        self.platformType = platformType
    }
}
