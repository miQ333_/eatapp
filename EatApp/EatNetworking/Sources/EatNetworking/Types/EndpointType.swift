public enum EndpointType {
    case refreshToken
    case registerUser
    case addToken
    
    public var urlPath: String {
        switch self {
        case .refreshToken:
            return "auth/apiToken"
        case .registerUser:
            return "auth/registerAnonymousUser"
        case .addToken:
            return "notificationToken"
        }
    }
}
