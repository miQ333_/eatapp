import Foundation
import Combine
import EatCommon

extension RestAPI {
    func get(endpoint: String, requestModifier: @escaping RequestModifier = { $0 }, fromSignalAPI: Bool = true) -> URLSession.ErasedDataTaskPublisher {
        guard let url = fromSignalAPI ? self.createUrl(endpoint: endpoint) : URL(string: endpoint) else {
            return Fail<URLSession.DataTaskPublisher.Output, Error>(error: AppError.unableToCreateURL).eraseToAnyPublisher()
        }
        
        let request = URLRequest(url: url)
        return createPublisher(for: request, requestModifier: requestModifier)
    }
    
    func put(endpoint: String, body: Data?, requestModifier: @escaping RequestModifier = { $0 }) -> URLSession.ErasedDataTaskPublisher {
        guard let url = self.createUrl(endpoint: endpoint) else {
            return Fail<URLSession.DataTaskPublisher.Output, Error>(error: AppError.unableToCreateURL).eraseToAnyPublisher()
        }
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.httpBody = body
        return createPublisher(for: request, requestModifier: requestModifier)
    }
    
    func post(endpoint: String, body: Data?, requestModifier: @escaping RequestModifier = { $0 }) -> URLSession.ErasedDataTaskPublisher {
        guard let url = self.createUrl(endpoint: endpoint) else {
            return Fail<URLSession.DataTaskPublisher.Output, Error>(error: AppError.unableToCreateURL).eraseToAnyPublisher()
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = body
        return createPublisher(for: request, requestModifier: requestModifier)
    }
    
    func delete(endpoint: String, requestModifier: @escaping RequestModifier = { $0 }) -> URLSession.ErasedDataTaskPublisher {
        guard let url = self.createUrl(endpoint: endpoint) else {
            return Fail<URLSession.DataTaskPublisher.Output, Error>(error: AppError.unableToCreateURL).eraseToAnyPublisher()
        }
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        return createPublisher(for: request, requestModifier: requestModifier)
    }
    
    func patch(endpoint: String, body: Data?, requestModifier: @escaping RequestModifier = { $0 }) -> URLSession.ErasedDataTaskPublisher {
        guard let url = self.createUrl(endpoint: endpoint) else {
            return Fail<URLSession.DataTaskPublisher.Output, Error>(error: AppError.unableToCreateURL).eraseToAnyPublisher()
        }
        var request = URLRequest(url: url)
        request.httpMethod = "PATCH"
        request.httpBody = body
        return createPublisher(for: request, requestModifier: requestModifier)
    }
    
    func createPublisher(for request: URLRequest, requestModifier: @escaping RequestModifier) -> URLSession.ErasedDataTaskPublisher {
        Just(request).setFailureType(to: Error.self)
            .flatMap { request -> URLSession.ErasedDataTaskPublisher in
                return self.urlSession.erasedDataTaskPublisher(for: requestModifier(request))
            }.eraseToAnyPublisher()
    }
    
    public func createUrl(endpoint: String) -> URL? {
        return URL(string: self.baseURL + endpoint)
    }
}
