import Foundation

public protocol RestAPI {
    typealias RequestModifier = ((URLRequest) -> URLRequest)
    
    var baseURL: String { get }
    var urlSession: URLSession { get }
}
