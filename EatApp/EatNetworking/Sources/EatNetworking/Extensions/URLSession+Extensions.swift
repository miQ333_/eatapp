import Foundation
import Combine
import EatCommon

extension URLSession {
    typealias ErasedDataTaskPublisher = AnyPublisher<(data: Data, response: URLResponse), Error>
    
    func erasedDataTaskPublisher(
        for request: URLRequest
    ) -> ErasedDataTaskPublisher {
        dataTaskPublisher(for: request)
            .mapError { $0 }
            .eraseToAnyPublisher()
    }
}


extension URLSession.ErasedDataTaskPublisher {
    func retryOnceOnUnauthorizedResponse(chainedRequest: AnyPublisher<Output, Error>? = nil) -> AnyPublisher<Output, Error> {
            tryMap { data, response -> URLSession.ErasedDataTaskPublisher.Output in
                if let res = response as? HTTPURLResponse,
                   res.statusCode == 401 {
                    throw AppError.unauthorized
                }
                return (data:data, response:response)
            }
            .retryOn(AppError.unauthorized, retries: 1, chainedPublisher: chainedRequest)
            .eraseToAnyPublisher()
    }
    
    func handleResponse<T: Decodable>() -> AnyPublisher<Result<T, AppError>, Never> {
        tryMap { data, response -> Data in
            guard let httpResponse = response as? HTTPURLResponse else {
               throw AppError.unknown
            }
            
            guard 200..<300 ~= httpResponse.statusCode else {
                throw AppError.init(rawValue: httpResponse.statusCode) ?? AppError.unknown
            }

            return data
        }
        .decode(type: T.self, decoder: JSONDecoder())
        .mapError { error -> AppError in
            if error is DecodingError {
                return AppError.jsonParsing
            }
            
            if let error = error as? AppError {
                return error
            }
            
            return AppError.unknown
        }
        .map(Result.success)
        .catch { error in
            Just(.failure(error))
        }
        .eraseToAnyPublisher()
    }
}
