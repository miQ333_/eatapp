import EatCommon
import Foundation
import Combine

extension EatNetworkRepository {
    public func registerAnonymousUser(request: RegisterAnonymousUserRequest) -> AnyPublisher<Result<AppUserResponse, AppError>, Never> {
        let url = URLComponents(string: EndpointType.registerUser.urlPath)!
        
        let body = try? JSONEncoder().encode(request)
                         
        return self.post(endpoint: (url.url?.absoluteString ?? ""), body: body)
            .handleResponse()
            .eraseToAnyPublisher()
    }
}
