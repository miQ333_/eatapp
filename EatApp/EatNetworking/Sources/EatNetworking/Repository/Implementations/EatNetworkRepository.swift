import Foundation
import EatCommon
import Combine

public class EatNetworkRepository: NetworkRepository {
    let userDefaultsManager: UserDefaultsManager
    let keychainManager: KeychainManager
    public let baseURL: String
    public let urlSession: URLSession
    
    public init(urlSession: URLSession = URLSession.shared, userDefaultsManager: UserDefaultsManager, baseURL: String, keychainManager: KeychainManager) {
        self.urlSession = urlSession
        self.userDefaultsManager = userDefaultsManager
        self.baseURL = baseURL
        self.keychainManager = keychainManager
    }
    
    var refresh: URLSession.ErasedDataTaskPublisher {
        
        guard let refreshToken = self.keychainManager.get(item: .refreshToken) else {
            return Fail(error: AppError.unauthorized).eraseToAnyPublisher()
        }
        
        return self.post(endpoint: EndpointType.refreshToken.urlPath, body: self.requestBodyFrom(params: ["refresh": refreshToken]), requestModifier: {
                $0.addingDefaultHeaders()
            })
        .tryMap { data, response -> URLSession.ErasedDataTaskPublisher.Output in
            guard let httpResponse = response as? HTTPURLResponse else {
                throw AppError.unknown
            }
            
            guard 200..<300 ~= httpResponse.statusCode else {
                throw AppError.init(rawValue: httpResponse.statusCode) ?? AppError.unknown
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(AppUserResponse.self, from: data)
                self.saveTokens(accessToken: response.token, refreshToken: response.refreshToken)
            } catch {
                throw AppError.jsonParsing
            }
            return (data:data, response:response)
        }
        .eraseToAnyPublisher()
    }
    
    func getAuthorizedRequestModifier() -> RequestModifier {
        return {
            $0.addingBearerAuthorization(token: self.keychainManager.get(item: .authToken) ?? "")
                .addingDefaultHeaders()
        }
    }
    
    func requestBodyFrom(params: [String: Any]?) -> Data? {
        guard let params = params else { return nil }
        guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return nil
        }
        return httpBody
    }
    
    func saveTokens(accessToken: String, refreshToken: String?) {
        if let _ = self.keychainManager.get(item: .authToken) {
            self.keychainManager.remove(item: .authToken)
        }
        self.keychainManager.add(value: accessToken, item: .authToken)
        
        if let token = refreshToken {
            if let _ = self.keychainManager.get(item: .refreshToken) {
                self.keychainManager.remove(item: .refreshToken)
            }
            self.keychainManager.add(value: token, item: .refreshToken)
        }
    }
    
}
