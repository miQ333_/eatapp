import EatCommon
import Foundation
import Combine

extension EatNetworkRepository {
    public func addToken(request: AddTokenRequest) -> AnyPublisher<Result<EmptyResponse, AppError>, Never> {
        let url = URLComponents(string: EndpointType.addToken.urlPath)!
        
        let body = try? JSONEncoder().encode(request)
                         
        return self.post(endpoint: (url.url?.absoluteString ?? ""), body: body, requestModifier: self.getAuthorizedRequestModifier())
            .handleResponse()
            .eraseToAnyPublisher()
    }
}
