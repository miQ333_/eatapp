import Combine
import EatCommon

public protocol NetworkRepository: RestAPI {
    func registerAnonymousUser(request: RegisterAnonymousUserRequest) -> AnyPublisher<Result<AppUserResponse, AppError>, Never>
    func addToken(request: AddTokenRequest) -> AnyPublisher<Result<EmptyResponse, AppError>, Never>
}
