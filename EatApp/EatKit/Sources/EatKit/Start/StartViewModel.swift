import EatCommon
import EatNetworking
import Combine
import Firebase

public enum StartNavigationType {
    case main(location: Location)
    case locations
}

public class StartViewModel: BaseViewModel {
    private let keychainManager: KeychainManager
    private let networkRepository: NetworkRepository
    private let userDefaultsManager: UserDefaultsManager
    
    private var subscriptions = Set<AnyCancellable>()
    
    public var navigationPublisher: AnyPublisher<StartNavigationType, Never> {
        self.navigationSubject.eraseToAnyPublisher()
    }
    private let navigationSubject = PassthroughSubject<StartNavigationType, Never>()
    
    public init(keychainManager: KeychainManager, networkRepository: NetworkRepository, userDefaultsManager: UserDefaultsManager) {
        self.keychainManager = keychainManager
        self.networkRepository = networkRepository
        self.userDefaultsManager = userDefaultsManager
    }
    
    public func setupViewModel() {
        self.requestTokenIfNeeded()
    }
    
    private func requestTokenIfNeeded() {
        if !self.isUserLogged() {
            self.registerNewUser()
        } else {
            self.navigate()
        }
    }
    
    private func isUserLogged() -> Bool {
        return self.keychainManager.get(item: .authToken) != nil
    }
    
    private func registerNewUser() {
        self.networkRepository.registerAnonymousUser(request: RegisterAnonymousUserRequest(platformType: "ios"))
            .sink(receiveValue: { [weak self] result in
                guard let strongSelf = self else { return }
                switch result {
                case .failure(let error):
                    strongSelf.errorMessageSubject.send(error)
                case .success(let response):
                    strongSelf.saveAppUserData(data: response)
                    strongSelf.setRegistrationToken()
                }
            })
            .store(in: &self.subscriptions)
    }
    
    private func saveAppUserData(data: AppUserResponse) {
        self.keychainManager.add(value: data.token, item: .authToken)
        self.keychainManager.add(value: data.refreshToken, item: .refreshToken)
    }
    
    private func setRegistrationToken() {
        InstanceID.instanceID().instanceID { (result, error) in
            guard error != nil else {
                self.errorMessageSubject.send(.unknown)
                return
            }

            let token = result.token

            self.networkRepository.addToken(request: AddTokenRequest(token: token))
                .sink(receiveValue: { [weak self] result in
                    guard let strongSelf = self else { return }
                    switch result {
                    case .failure(let error):
                        strongSelf.errorMessageSubject.send(error)
                    case .success(_):
                        strongSelf.userDefaultsManager.saveNotificationToken(token)
                        strongSelf.navigate()
                    }
                })
                .store(in: &self.subscriptions)
        }
    }
    
    private func navigate() {
        if let location = self.getDefaultLocation() {
            self.navigationSubject.send(.main(location: location))
        } else {
            self.navigationSubject.send(.locations)
        }
    }
    
    private func getDefaultLocation() -> Location? {
        return self.userDefaultsManager.getUserDefaultLocation()
    }
}
