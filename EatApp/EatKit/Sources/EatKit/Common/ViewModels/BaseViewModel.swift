import Combine
import EatCommon

public class BaseViewModel {
    public var errorMessagePublisher: AnyPublisher<AppError, Never> {
        self.errorMessageSubject.eraseToAnyPublisher()
    }
    internal let errorMessageSubject = PassthroughSubject<AppError, Never>()
}
