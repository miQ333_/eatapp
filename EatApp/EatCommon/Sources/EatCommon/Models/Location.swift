//
//  File.swift
//  
//
//  Created by Marek Przybolewski on 14/12/2021.
//

import Foundation

public struct Location {
    public let id: String
    public let buildingName: String
    public let company: String?
    public let city: String
    public let address: String
    public let additionalInfo: String?
    public let name: String?

    init (id: String, buildingName: String, company: String?, city: String, address: String, additionalInfo: String?, name: String?) {
        self.id = id
        self.buildingName = buildingName
        self.company = company
        self.city = city
        self.address = address
        self.additionalInfo = additionalInfo
        self.name = name
    }
}
