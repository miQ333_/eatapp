import Foundation

public enum KeychainItemType {
    case authToken
    case refreshToken
    
    var key: String {
        switch self {
        case .authToken:
            return "UserManagerKeychainAuthTokenAccount"
        case .refreshToken:
            return "UserManagerKeychainRefreshToken"
        }
    }
}
