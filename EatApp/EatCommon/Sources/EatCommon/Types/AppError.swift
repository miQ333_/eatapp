public enum AppError: Int, Error {
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
    case notFound = 404
    
    case unknown = 900
    case unableToCreateURL = 1014
    case jsonParsing = 1015
}
