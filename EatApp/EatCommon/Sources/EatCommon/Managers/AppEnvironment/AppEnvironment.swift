public protocol AppEnvironment {
    var appName: String { get set }
    var baseURL: String { get set }
}
