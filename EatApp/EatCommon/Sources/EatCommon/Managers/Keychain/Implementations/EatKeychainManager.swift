import Foundation

public class EatKeychainManager: KeychainManager {
    let keychainAccessGroupName = "eatKeychainGroup"
    
    let appName: String
    
    public init(appName: String) {
        self.appName = appName
    }
    
    public func get(item: KeychainItemType) -> String? {
        return self.loadItemWith(key: item.key)
    }
    
    public func remove(item: KeychainItemType) {
        if get(item: item) != nil {
            self.deleteItemWith(key: item.key)
        }
    }
    
    public func add(value: String, item: KeychainItemType) {
        self.add(value: value, key: item.key)
    }
    
    public func cleanUpKeychain() {
        self.remove(item: .authToken)
        self.remove(item: .refreshToken)
    }
    
    private func deleteItemWith(key: String) {
        let queryDelete: [String : AnyObject] = [
            kSecClass as String : kSecClassGenericPassword,
            kSecAttrAccount as String : (key + self.appName) as AnyObject,
            kSecAttrAccessGroup as String : self.keychainAccessGroupName as AnyObject
        ]
        
        let resultCodeDelete = SecItemDelete(queryDelete as CFDictionary)
        
        if resultCodeDelete != noErr {
            print("Error deleting from Keychain: \(resultCodeDelete)")
        }
    }
    
    private func add(value: String, key: String) {
        guard let valueData = value.data(using: String.Encoding.utf8) else {
            print("Error saving text to Keychain")
            return
        }
        
        let queryAdd: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: (key + self.appName) as AnyObject,
            kSecValueData as String: valueData as AnyObject,
            kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked,
            kSecAttrAccessGroup as String: self.keychainAccessGroupName as AnyObject
        ]
        
        let resultCode = SecItemAdd(queryAdd as CFDictionary, nil)
        
        if resultCode != noErr {
            print("Error saving to Keychain: \(resultCode)")
        }
    }
    
    private func loadItemWith(key: String) -> String? {
        let queryLoad: [String: AnyObject] = [
            kSecClass as String : kSecClassGenericPassword,
            kSecAttrAccount as String : (key + self.appName) as AnyObject,
            kSecReturnData as String : kCFBooleanTrue,
            kSecMatchLimit as String : kSecMatchLimitOne,
            kSecAttrAccessGroup as String : self.keychainAccessGroupName as AnyObject
        ]
        
        var result: AnyObject?
        
        let resultCodeLoad = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(queryLoad as CFDictionary, UnsafeMutablePointer($0))
        }
        
        if resultCodeLoad == noErr {
            if let result = result as? Data, let keyValue = NSString(data: result, encoding: String.Encoding.utf8.rawValue) as String? {
                return keyValue
            }
        } else {
            print("Error loading from Keychain: \(resultCodeLoad)")
        }
        return nil
    }
}
