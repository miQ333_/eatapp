public protocol KeychainManager {
    func get(item: KeychainItemType) -> String?
    func remove(item: KeychainItemType)
    func add(value: String, item: KeychainItemType)
    func cleanUpKeychain()
}
