import Foundation

public class EatUserDefaultsManager: UserDefaultsManager {
    let userDefaultLocationKey = "UserDefaultLocationKey"
    
    public init() {}
    
    public func saveUserDefaultLocation(_ location: Location) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: location)
        UserDefaults.standard.set(encodedData, forKey: self.userDefaultLocationKey)
        UserDefaults.standard.synchronize()
    }
    
    public func getUserDefaultLocation() -> Location? {
        return self.getDataForKey(userDefaultLocationKey)
    }
    
    private func getDataForKey<T>(_ key: String) -> T? {
        do {
            if let retrievedData = try UserDefaults.standard.data(forKey: key) {
                guard let unarchivedObject = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(retrievedData)  else {
                    return nil
                }
                return unarchivedObject as? T
            }

            if let retrievedData = try UserDefaults.standard.object(forKey: key) as? T {
                return retrievedData
            }
            return nil
        } catch {
            return nil
        }

    }
}
