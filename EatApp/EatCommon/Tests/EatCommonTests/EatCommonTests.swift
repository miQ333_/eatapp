import XCTest
@testable import EatCommon

final class EatCommonTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(EatCommon().text, "Hello, World!")
    }
}
